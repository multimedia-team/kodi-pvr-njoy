kodi-pvr-njoy (21.0.2+ds-1) unstable; urgency=high

  * New upstream version 21.0.2+ds
  * d/copyright: Bump copyright years

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 27 Jan 2025 08:46:06 +0000

kodi-pvr-njoy (21.0.1+ds-2) unstable; urgency=critical

  * Mass fix: d/watch: Allow multiple addon suites to be checked

 -- Vasyl Gello <vasek.gello@gmail.com>  Tue, 20 Aug 2024 12:13:57 +0000

kodi-pvr-njoy (21.0.1+ds-1) unstable; urgency=medium

  * New upstream version 21.0.1+ds

 -- Vasyl Gello <vasek.gello@gmail.com>  Fri, 09 Aug 2024 07:57:30 +0000

kodi-pvr-njoy (21.0.0+ds-1) experimental; urgency=medium

  * New upstream version 21.0.0+ds
  * Branch out experimental
  * Modernize package
  * New release 20.2.0+ds1-1
  * Finalize changelog
  * d/gbp.conf: Fix debian and upstream branches
  * Import Debian packaging from debian/sid
  * Prepare for v21 "Omega"
  * Bump copyright years
  * d/control: Ensure Vcs-Git points to correct branch

 -- Vasyl Gello <vasek.gello@gmail.com>  Sat, 09 Mar 2024 15:10:18 +0000

kodi-pvr-njoy (20.3.0+ds1-1) unstable; urgency=medium

  * New upstream version 20.3.0+ds1
  * d/watch: switch to git tags

 -- Vasyl Gello <vasek.gello@gmail.com>  Sat, 15 Oct 2022 16:56:37 +0000

kodi-pvr-njoy (20.2.0+ds1-1) unstable; urgency=medium

  * New upstream version 20.2.0+ds1
  * Prepare for v20 in unstable

 -- Vasyl Gello <vasek.gello@gmail.com>  Thu, 04 Aug 2022 09:57:13 +0000

kodi-pvr-njoy (19.0.1+ds1-1) unstable; urgency=medium

  * New upstream version 19.0.1+ds1
  * Modernize package

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 21 Mar 2022 17:51:07 +0000

kodi-pvr-njoy (19.0.0+ds1-1) unstable; urgency=medium

  * New upstream version 19.0.0+ds1
  * Bump standard version
  * Disable LTO to make build reproducible

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 13 Oct 2021 22:14:01 +0000

kodi-pvr-njoy (7.1.1+ds1-1) unstable; urgency=medium

  * New upstream version 7.1.1+ds1
  * Fix github ref
  * Restrict watchfile to current stable Kodi codename

 -- Vasyl Gello <vasek.gello@gmail.com>  Sun, 15 Aug 2021 21:32:43 +0000

kodi-pvr-njoy (7.1.0+ds1-1) unstable; urgency=medium

  * New upstream version 7.1.0+ds1
  * Force override libdir paths
  * Bump copyright years

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 20 Jan 2021 06:24:01 +0000

kodi-pvr-njoy (7.0.0+ds1-1) unstable; urgency=medium

  [ Vasyl Gello ]
  * New upstream version 7.0.0+ds1
    + Repack out vendored dependencies
  * d/rules:
    + Use architecture.mk for DEB_HOST_MULTIARCH
    + Enable hardening flags
  * Update d/watch
  * d/control:
    + Remove Balint Reczey from Uploaders per his wish, and add myself
    + Use dh-sequence-kodiaddon
    + Drop kodiplatform and add libjsoncpp-dev / pkg-config build-dependencies
    + Update Vcs-* for the move to 'kodi-media-center' subgroup
    + Bump Standards-Version to 4.5.1, no changes needed
    + Bump debhelper compat level to 13, use debhelper-compat and drop d/compat
  * Update d/copyright
  * Add new d/upstream/metadata

  [ Debian Janitor ]
  * Mark kodi-pvr-njoy as Multi-Arch: same.

 -- Vasyl Gello <vasek.gello@gmail.com>  Sun, 20 Dec 2020 22:09:30 +0000

kodi-pvr-njoy (3.4.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Balint Reczey ]
  * Add basic Salsa CI configuration
  * Add debian/watch
  * New upstream version 3.4.2
  * Reresh patches
  * Bump libkodiplatform-dev and kodi-addons-dev build-dependency versions

 -- Balint Reczey <rbalint@ubuntu.com>  Sat, 21 Mar 2020 16:38:27 +0100

kodi-pvr-njoy (2.4.0+git20160518-2) unstable; urgency=medium

  * Really depend on PVR API version of Kodi we built the addon with
  * Adapt to Kodi PVR api change - SeekTime

 -- Balint Reczey <balint@balintreczey.hu>  Sat, 24 Dec 2016 03:51:34 +0100

kodi-pvr-njoy (2.4.0+git20160518-1) unstable; urgency=medium

  * Imported Upstream version 2.4.0+git20160518
  * Build-depend on latest libkodiplatform-dev and kodi-addons-dev
  * Build-depend on kodi-addons-dev version which has all the needed headers
    (Closes: #844969)
  * Drop hack to build with p8-platform
  * Drop unused obsoleted patch
  * Depend on PVR API version of Kodi we built the addon with
  * Bump standards version to 3.9.8

 -- Balint Reczey <balint@balintreczey.hu>  Sat, 19 Nov 2016 08:49:49 +0100

kodi-pvr-njoy (1.11.9+git20160210-1) unstable; urgency=medium

  * Imported Upstream version 1.11.9+git20160210
  * Bump standards version to 3.9.7
  * Switch to p8-platform from (cec-)platform
  * Extend package description (Closes: #808750)

 -- Balint Reczey <balint@balintreczey.hu>  Thu, 03 Mar 2016 14:34:28 +0100

kodi-pvr-njoy (1.10.7+git20150717-1) unstable; urgency=medium

  * Create packaging compliant to Debian Policy

 -- Balint Reczey <balint@balintreczey.hu>  Sat, 12 Dec 2015 20:31:53 +0100

kodi-pvr-njoy (1.9.27-1~trusty) trusty; urgency=low

  [ kodi ]
  * autogenerated dummy changelog

 -- Nobody <nobody@kodi.tv>  Sat, 01 Jun 2013 00:59:22 +0200
